package com.interview.springboot.controller;

import com.interview.springboot.model.entity.User;
import com.interview.springboot.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(origins = "", maxAge = 3600)
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping(value = "/user/{id}")
    public ResponseEntity getUser(@PathVariable(value = "id") int id){
        return userService.getUserById(id);
    }

    @GetMapping(value = "/users")
    public ResponseEntity getUsers(){
        return userService.getAllUsers();
    }

    @PostMapping(value = "/user-add", headers = "Accept=application/json")
    public ResponseEntity setUser(@Valid @RequestBody User user){
        return userService.setUser(user);
    }

    @PutMapping(value = "/user-update/{id}")
    public ResponseEntity updateUser(@PathVariable int id, @Valid @RequestBody User newUser){
        return userService.updateUser(id, newUser);
    }

    @DeleteMapping(value = "/user-delete/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable int id){
        return userService.deleteUser(id);
    }

}
