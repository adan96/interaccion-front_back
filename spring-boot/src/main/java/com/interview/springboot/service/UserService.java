package com.interview.springboot.service;

import com.interview.springboot.model.entity.User;
import com.interview.springboot.model.entity.Response;
import com.interview.springboot.model.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class UserService {

    //private static final Logger LOG = Logger.getLogger(UserService.class.getName());
    //private static final String URL = "http://localhost:8002";

    @Autowired
    private UserRepository userRepository;

    public ResponseEntity getUserById(int id) {
        try {
            User user =userRepository.findById(id);
            if (user == null) {
                //LOG.log(Level.INFO, URL + "/user/" + id + " [" + HttpStatus.NOT_FOUND + "]");
                return new ResponseEntity<>(
                        new Response(
                                HttpStatus.NOT_FOUND, "Cannot find that user with id: " + id), HttpStatus.NOT_FOUND);
            } else{
                //LOG.log(Level.INFO, URL + "/user/" + id + " [" + HttpStatus.OK + "]");
                return new ResponseEntity(user, HttpStatus.OK);
            }
        } catch (Exception e) {
            //LOG.log(Level.INFO, URL + "/user/" + id + " [" + HttpStatus.BAD_REQUEST + "]");
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST, "An error has been occurred."), HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity getAllUsers(){
        try {
            List<User> users = userRepository.findAll();
            if(users.isEmpty()){
                //LOG.log(Level.INFO, URL + "/users [" + HttpStatus.NOT_FOUND + "]");
                return new ResponseEntity<>(
                        new Response(
                                HttpStatus.NOT_FOUND,"No Users can be found"), HttpStatus.NOT_FOUND);
            }else {
                //LOG.log(Level.INFO, URL + "/users [" + HttpStatus.OK + "]");
                return new ResponseEntity(users, HttpStatus.OK);
            }
        }catch (Exception e){
            //LOG.log(Level.INFO, URL + "/users [" + HttpStatus.BAD_REQUEST + "]");
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST ,"An error has been occurred."), HttpStatus.BAD_REQUEST);
        }



    }

    public ResponseEntity<?> setUser(User user) {
        try {
            userRepository.save(user);
        } catch (Exception e) {
            //LOG.log(Level.INFO, URL + "/user-add [" + HttpStatus.BAD_REQUEST + "]");
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST, "An error has been occurred."), HttpStatus.BAD_REQUEST);
        }
        //LOG.log(Level.INFO, URL + "/user-add [" + HttpStatus.CREATED + "]");
        return new ResponseEntity<>(
                new Response(
                        HttpStatus.CREATED, "User added successfully."), HttpStatus.CREATED);
    }

    public ResponseEntity updateUser(int id, User newUser) {
        User oldUser = userRepository.findById(id);
        if (oldUser == null) {
            //LOG.log(Level.INFO, URL + "/user-update/" + id + " [" + HttpStatus.NOT_FOUND + "]");
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.NOT_FOUND, "Cannot find that user with id: " + id), HttpStatus.NOT_FOUND);
        }
        try {
            userRepository.save(updateUser(oldUser, newUser));
        } catch (Exception e) {
            //LOG.log(Level.INFO, URL + "/user-update/" + id + " [" + HttpStatus.BAD_REQUEST + "]");
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST, "Cannot upgrade that user with id: " + id), HttpStatus.BAD_REQUEST);
        }
        //LOG.log(Level.INFO, URL + "/user-update/" + id + " [" + HttpStatus.ACCEPTED + "]");
        return new ResponseEntity<>(
                new Response(
                        HttpStatus.ACCEPTED, "User updated successfully"), HttpStatus.ACCEPTED);
    }

    public ResponseEntity<?> deleteUser(int id) {
        User user = userRepository.findById(id);
        if (user == null) {
            //LOG.log(Level.INFO, URL + "/user-delete/" + id + " [" + HttpStatus.NOT_FOUND + "]");
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.NOT_FOUND, "Cannot find that user with id: " + id), HttpStatus.NOT_FOUND);
        }
        try {
            userRepository.delete(user);
        } catch (Exception e) {
            //LOG.log(Level.INFO, URL + "/user-delete/" + id + " [" + HttpStatus.BAD_REQUEST + "]");
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST, "Cannot delete that user with id: " + id), HttpStatus.BAD_REQUEST);
        }
        //LOG.log(Level.INFO, URL + "/user-delete/" + id + " [" + HttpStatus.ACCEPTED + "]");
        return new ResponseEntity<>(
                new Response(
                        HttpStatus.ACCEPTED, "User deleted successfully"), HttpStatus.ACCEPTED);
    }

    private User updateUser(User oldUser, User newUser) {

        if(newUser.getNombre() != null)
            oldUser.setNombre(newUser.getNombre());
        if(newUser.getApellido() != null)
            oldUser.setApellido(newUser.getApellido());
        if(newUser.getCorreo() != null)
            oldUser.setCorreo(newUser.getCorreo());

        return oldUser;
    }

}
