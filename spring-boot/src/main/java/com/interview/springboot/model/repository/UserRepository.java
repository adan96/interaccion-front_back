package com.interview.springboot.model.repository;

import com.interview.springboot.model.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository <User, Integer> {

    @Query("SELECT s FROM User s WHERE s.id = ?1")
    User findById(@Param("id") int id);

    @Query("SELECT s FROM User s WHERE s.id > 0")
    List<User> findAll();

}
