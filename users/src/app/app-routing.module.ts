import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MostrarComponent } from './User/mostrar/mostrar.component';
import { AgregarComponent } from './User/agregar/agregar.component';
import { ModificarComponent } from './User/modificar/modificar.component';


const routes: Routes = [
  {path: 'mostrar', component: MostrarComponent},
  {path: 'agregar', component: AgregarComponent},
  {path: 'modificar', component: ModificarComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
