import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './Model/User';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private http: HttpClient) { }

  Url="http://localhost:8001/";


  getUserById(id: number){
    return this.http.get<User>(this.Url + "user/" + id);
  }

  getAllUsers(){
    return this.http.get<User[]>(this.Url + "users");
  }

  postUser(user: User){
    return this.http.post<User>(this.Url + "user-add", user);
  }

  putUser(user: User){
    return this.http.put<User>(this.Url + "user-update/" + user.id, user);
  }

  deleteUser(user: User){
    return this.http.delete<User>(this.Url + "user-delete/" + user.id);
  }

}
