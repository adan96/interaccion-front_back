import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from '../../service.service';
import { User } from '../../Model/User';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.css']
})
export class AgregarComponent implements OnInit {

  user: User = new User();

  constructor(private router: Router, private service: ServiceService) { 
  }

  ngOnInit(): void {
  }

  guardar(){
    this.service.postUser(this.user).subscribe(data => {
      this.router.navigate(["mostrar"]);
    })
  }

}
