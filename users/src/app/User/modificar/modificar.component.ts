import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../../service.service';
import { Router } from '@angular/router';
import { User } from '../../Model/User';

@Component({
  selector: 'app-modificar',
  templateUrl: './modificar.component.html',
  styleUrls: ['./modificar.component.css']
})
export class ModificarComponent implements OnInit {

  user: User = new User();
  constructor(private router: Router, private service: ServiceService) { }

  ngOnInit(): void {
    this.editar();
  }

  editar(){
    let id = localStorage.getItem("id");
    var a  = 5;
    this.service.getUserById(+id).subscribe(data => {
      this.user = data;
    })
  }

  actualizar(user: User){
    this.service.putUser(user)
    .subscribe(data => {
      this.user = data;
      this.router.navigate(["mostrar"]);
    })
  }

}
