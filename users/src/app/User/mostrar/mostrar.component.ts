import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {ServiceService} from '../../service.service';
import { User } from '../../Model/User';

@Component({
  selector: 'app-mostrar',
  templateUrl: './mostrar.component.html',
  styleUrls: ['./mostrar.component.css']
})
export class MostrarComponent implements OnInit {

  users: User[];
  constructor(private service: ServiceService, private router: Router) { }

  ngOnInit(): void {
    this.service.getAllUsers().subscribe(data => {
      this.users = data;
    })
  }

  modificar(user: User):void{
    localStorage.setItem("id", user.id.toString());
    this.router.navigate(["modificar"]);
  }

  eliminar(user: User){
    this.service.deleteUser(user).subscribe(data => {
      this.users = this.users.filter(u => u !== user);
    })
  }

}
